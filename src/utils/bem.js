export default styles => {
  return (block, element, mods) => {
    /**
     * Нормализация аргументов
     */
    if (element) {
      if (!mods) {
        if (element.constructor === Array) {
          for (let mod of element) {
            mods[mod] = true
          }
          element = undefined
        }
        if (element.constructor === Object) {
          mods = element
          element = undefined
        }
      }
    }

    let baseClass = block
    if (element) baseClass = `${baseClass}__${element}`

    const classes = [styles[baseClass]]

    if (mods) {
      const keys = Object.keys(mods)
      const values = Object.values(mods)

      for (let idx in keys) {
        const key = keys[idx]
        const value = values[idx]

        if (
          value === true ||
          value === 'true'
        ) {
          classes.push(
            styles[`${baseClass}_${key}`]
          )
        } else {
          classes.push(
            styles[`${baseClass}_${key}_${value}`]
          )
        }
      }
    }

    return classes.filter(item => item).join(' ')
  }
}
