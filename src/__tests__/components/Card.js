import Card from '@components/Card'

describe('Card',()=>{
  it('check snapshot', () => {
    const cardWrapper = shallow(
        <Card
          idx={0}
          name="club_jack"
          status="BACK"
          onClick={()=>{}}
        />
    );
    expect(cardWrapper).toMatchSnapshot()
  })

  it('should fire props onClick function after click on card', () => {
    const onClick = jest.fn()
    const cardWrapper = shallow(
        <Card
          idx={0}
          name="club_jack"
          status="BACK"
          onClick={onClick}
        />
    )
    cardWrapper.find('.card__handler').simulate('click')
    expect(onClick).toHaveBeenCalled()
  })
})
