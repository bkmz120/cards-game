import CardsGame from '@components/CardsGame'
import CardsField from '@components/CardsField'
import Timer from '@components/Timer'
import * as cards from '@utils/cards'
import * as cardStatus from '@constants/cardStatus'
import * as timerStatus from '@constants/timerStatus'

jest.useFakeTimers();

const generatedCardsList = [
  {"name":"club_jack","status":"BACK"},
  {"name":"club_jack","status":"BACK"},
  {"name":"diamond_5","status":"BACK"},
  {"name":"diamond_5","status":"BACK"}
]

cards.generateCards = jest.fn().mockReturnValue(generatedCardsList)

describe('CardsGame',()=>{
  it('should generate cards and render CardsField with cards in props', () => {
    const cardsGameWrapper = shallow(
        <CardsGame />
    );
    const cardsField = cardsGameWrapper.find(CardsField)
    expect(cardsField.exists()).toEqual(true)
    expect(cardsField.props().cards).toEqual(generatedCardsList);
  })

  it('if handleCardClick fired once should set status FRONT to card with index == idx', () => {
    const cardsGameWrapper = shallow(
        <CardsGame />
    );
    const idx = 0
    cardsGameWrapper.instance().handleCardClick(idx)
    const cardsField = cardsGameWrapper.find(CardsField)
    expect(cardsField.props().cards[idx].status).toEqual(cardStatus.FRONT)
  })

  it('if handleCardClick fired twice with the same idx should not change cards list after second fire', () => {
    const cardsGameWrapper = shallow(
        <CardsGame />
    );
    const idx = 0
    cardsGameWrapper.instance().handleCardClick(idx)
    const cards = [...cardsGameWrapper.find(CardsField).props().cards]
    cardsGameWrapper.instance().handleCardClick(idx)
    expect(cardsGameWrapper.find(CardsField).props().cards).toEqual(cards)
  })

  it('if handleCardClick fired twice with different idx and names of this cards are the same should set status HIDDEN to both cards', () => {
    const cardsGameWrapper = shallow(
        <CardsGame />
    );
    const idx1 = 0,
          idx2 = 1
    cardsGameWrapper.instance().handleCardClick(idx1)
    cardsGameWrapper.instance().handleCardClick(idx2)
    jest.runAllTimers();
    const cards = cardsGameWrapper.find(CardsField).props().cards
    expect(cards[idx1].status).toEqual(cardStatus.HIDDEN)
    expect(cards[idx2].status).toEqual(cardStatus.HIDDEN)
  })

  it('if handleCardClick fired twice with different idx and names of this cards are different should set status BACK to both cards', () => {
    const cardsGameWrapper = shallow(
        <CardsGame />
    );
    const idx1 = 0,
          idx2 = 2
    cardsGameWrapper.instance().handleCardClick(idx1)
    cardsGameWrapper.instance().handleCardClick(idx2)
    jest.runAllTimers();
    const cards = cardsGameWrapper.find(CardsField).props().cards
    expect(cards[idx1].status).toEqual(cardStatus.BACK)
    expect(cards[idx2].status).toEqual(cardStatus.BACK)
  })

  it('if opened the same cards should render incremented score', () => {
    const cardsGameWrapper = shallow(
        <CardsGame />
    );
    const idx1 = 0,
          idx2 = 1
    const initScoreValue = cardsGameWrapper.find('.cards__score-value').text()
    cardsGameWrapper.instance().handleCardClick(idx1)
    cardsGameWrapper.instance().handleCardClick(idx2)
    expect(initScoreValue).toEqual("0")
    expect(cardsGameWrapper.find('.cards__score-value').text()).toEqual("1")
  })

  it('should pass to Timer props status RESET on init and status ON after first card was opened', () => {
    const cardsGameWrapper = shallow(
        <CardsGame />
    );
    const idx1 = 0;
    const timer = cardsGameWrapper.find(Timer)
    expect(timer.exists()).toEqual(true)
    expect(timer.props().status).toEqual(timerStatus.RESET)
    cardsGameWrapper.instance().handleCardClick(idx1)
    expect(cardsGameWrapper.find(Timer).props().status).toEqual(timerStatus.ON)
  })

  it('should render finish text after all pairs of cards will be opened', () => {
    const cardsGameWrapper = shallow(
        <CardsGame />
    );
    cardsGameWrapper.instance().pairsNumber = 2
    cardsGameWrapper.instance().handleCardClick(0)
    cardsGameWrapper.instance().handleCardClick(1)
    jest.runAllTimers()
    cardsGameWrapper.instance().handleCardClick(2)
    cardsGameWrapper.instance().handleCardClick(3)
    expect(cardsGameWrapper.find('.cards__finish-text').exists()).toEqual(true)
  })

  it('should pass to Timer props status OFF after all pairs of cards will be opened', () => {
    const cardsGameWrapper = shallow(
        <CardsGame />
    );
    cardsGameWrapper.instance().pairsNumber = 2
    cardsGameWrapper.instance().handleCardClick(0)
    cardsGameWrapper.instance().handleCardClick(1)
    jest.runAllTimers()
    cardsGameWrapper.instance().handleCardClick(2)
    cardsGameWrapper.instance().handleCardClick(3)
    expect(cardsGameWrapper.find(Timer).props().status).toEqual(timerStatus.OFF)
  })

  it('should generate new cards, clear score and set to timer props status RESET after click on start new game btn', () => {
    const cardsGameWrapper = shallow(
        <CardsGame />
    );
    cardsGameWrapper.instance().handleCardClick(0)
    cardsGameWrapper.instance().handleCardClick(1)
    jest.runAllTimers()
    let cards = [...cardsGameWrapper.find(CardsField).props().cards]
    cardsGameWrapper.find('.cards__start-btn').simulate('click')
    expect(cardsGameWrapper.find(CardsField).props().cards).not.toEqual(cards)
    expect(cardsGameWrapper.find('.cards__score-value').text()).toEqual("0")
    expect(cardsGameWrapper.find(Timer).props().status).toEqual(timerStatus.RESET)
  })
})
