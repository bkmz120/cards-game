import React from 'react'
import PropTypes from 'prop-types'

import style from './style/index.css'
import Bem from '@utils/bem'
import Card from '@components/Card'

const b = Bem(style)

export default class CardsField extends React.PureComponent {
  render () {
    const {
      cards,
      onCardClick
    } = this.props
    return <div className={b('cardsField')}>
      { cards.map((card, idx) => (
        <Card
          key={idx}
          idx={idx}
          name={card.name}
          status={card.status}
          onClick={onCardClick}
        />
      ))}
    </div>
  }
}

CardsField.propTypes = {
  cards: PropTypes.array.isRequired,
  onCardClick: PropTypes.func.isRequired
}
