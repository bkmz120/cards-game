import React from 'react'
import PropTypes from 'prop-types'
import svgCards from 'svg-cards/svg-cards.svg'
import 'svgxuse/svgxuse.js'

import style from './style/index.css'
import Bem from '@utils/bem'
import * as cardStatus from '@constants/cardStatus'

const b = Bem(style)

export default class Card extends React.PureComponent {
  render () {
    const {
      idx,
      name,
      status,
      onClick
    } = this.props
    const isFlip = status === cardStatus.FRONT || status === cardStatus.HIDDEN
    return <div className={b('card')}>
      <div className={b('card', 'flipper', { hidden: status === cardStatus.HIDDEN })} >
        <div className={b('card', 'back-side', { flip: isFlip })} >
          <svg className={b('card', 'svg')} viewBox="0 0 59 85" width="59" height="85">
            <use xlinkHref={`${svgCards}#alternate-back`} className={b('card', 'svg-use')} transform='scale(0.35)' />
          </svg>
        </div>
        <div className={b('card', 'front-side', { flip: isFlip })} >
          <svg className={b('card', 'svg')} viewBox="0 0 59 85" width="59" height="85">
            <use xlinkHref={`${svgCards}#${name}`} className={b('card', 'svg-use')} transform='scale(0.35)'/>
          </svg>
        </div>
      </div>
      <div className={b('card', 'handler')} onClick={() => onClick(idx)}></div>
    </div>
  }
}

Card.propTypes = {
  idx: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}
