import React from 'react'

import style from './style/index.css'
import Bem from '@utils/bem'
import CardsGame from '@components/CardsGame'

const b = Bem(style)

export default class App extends React.Component {
  render () {
    return <div className={b('app')}>
      <CardsGame />
    </div>
  }
}
