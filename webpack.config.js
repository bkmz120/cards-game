'use strict';
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const eslintFriendlyFormatter = require("eslint-friendly-formatter");

module.exports = function(env = {}, argv) {
  return {
    entry: [
      'babel-polyfill',
      path.join(__dirname, 'src/index.js'),
    ],
    output: {
      path: path.join(__dirname, 'build'),
      publicPath: '/',
      filename: 'assets/app.[hash].js',
    },
    module: {
      rules: [
        {
          enforce: "pre",
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          loader: "eslint-loader",
          options: {
            formatter: eslintFriendlyFormatter,
            fix: true,
          }
        },
        {
          test: /\.(js|jsx)$/,
          use: ["babel-loader"],
          exclude: /node_modules/,
        },
        {
          test: /\.css/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                modules: true,
                localIdentName: '[local]--[hash:base64:5]',
                importLoaders: 1,
              },
            },
            'postcss-loader'
          ]
        },
        {
          test: /\.(jpg|jpeg|png|webp|svg)(\?.*)?$/,
          use: [{
            loader: 'url-loader',
            options: {
              name: '[name].[hash:8].[ext]',
              outputPath: 'assets/images',
              publicPath: '/assets/images',
              limit: 8192,
            }
          }]
        },
        {
          test: /\.(woff|woff2)(\?.*)?$/,
          use: [{
            loader: 'file-loader',
            options: {
              name: '[name].[hash:8].[ext]',
              outputPath: 'assets/fonts',
              publicPath: '/assets/fonts',
              limit: 8192,
            }
          }]
        }
      ]
    },
    resolve: {
      extensions: ['*','.js', '.jsx'],
      alias: {
        '@app': path.resolve(__dirname, 'src/'),
        '@components': path.resolve(__dirname, 'src/components/'),
        '@utils': path.resolve(__dirname, 'src/utils/'),
        '@constants': path.resolve(__dirname, 'src/constants/'),
      }
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'assets/[name].[hash].css'
      }),
      new HtmlWebpackPlugin({
        template: 'src/index.html',
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeRedundantAttributes: true,
        },
      }),
    ],
    devServer: {
      inline: false,
      historyApiFallback: true,
    },
  };
}
